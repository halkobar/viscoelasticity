import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

# co bych chtěla doplnit/upravit
# xxx1) zatížení posunem
# 2) zatížení silou, která ale není konstantní (např harmonická) a totéž asi i pro posun
# 3) aby se mi vykreslovaly dva samostatné obrázky - F(t), u(t)
# 4) při vykreslování je občas začátek grafu hnusnej - jde to nějak upravit?


def my_sum(ii, jj):
    sum_out = 0
    for k in range(jj+1):
        sum_out += q[k][ii] * weight[len(q) - k][ii]
    return sum_out

################################################################################
################################################################################
# inputs
models_static_dynamic = [1]  # 0 - static, 1 - dynamic
models_load_type = [0]  # 0 - force, 1 - displacement

#properties of the system
models_moduli = [[5, 10]]  # MPa
models_characteristic_times = [[10**16, 0.1]]  # s

models_mass = [1]
models_delta_t = [0.001]  # s
time_total = 10  # seconds

# load settings
models_load_type_displacement = [2]  # 0 - initial, 1 - constant over time, 2 - harmonic
models_load_type_force = [0]  # 0 - initial, 1 - constant over time, 2 - harmonic

models_initial_displacement = [1]
models_external_force = [0]
models_frequency = [1]  # applicable only for harmonic loading

# derivative type
models_derivative_type = [0]  # 0 - standard derivative, 1 - fractional derivative
models_alpha = [[0, 1]]  # applicable only for fractional derivative
################################################################################
################################################################################


plt.figure(figsize=(7, 4.5))
for model_idx in range(len(models_moduli)):
    moduli = models_moduli[model_idx]
    characteristic_times = models_characteristic_times[model_idx]
    external_force = models_external_force[model_idx]
    mass = models_mass[model_idx]
    delta_t = models_delta_t[model_idx]
    initial_displacement = models_initial_displacement[model_idx]
    derivative_type = models_derivative_type[model_idx]
    alpha = models_alpha[model_idx]
    static_dynamic = models_static_dynamic[model_idx]
    load_type_displacement = models_load_type_displacement[model_idx]
    load_type_force = models_load_type_force[model_idx]
    frequency = models_frequency[model_idx]

    # initiation of arrays and variables
    n_time_steps = int(time_total / delta_t)
    q_0 = [0] * len(characteristic_times)
    x_values = np.linspace(0, delta_t * n_time_steps, num=int(n_time_steps+1))


# setting label of the graph
    # use exponential format for tau
    f = mticker.ScalarFormatter(useMathText=True)
    f.set_powerlimits((-3, 3))

    if derivative_type == 0:
        derivative_type_text = 'standard: '
    else:
        derivative_type_text = 'fractional: '
    current_label = derivative_type_text

    current_label += r'$E = $' + r'\{$%.1f$' % moduli[0]
    for modulus in moduli[1:]:
        current_label += r', $%.1f$' % modulus
    current_label += r'\} MPa'
    current_label += r'; $\tau = $' + r'\{' + r'${}$'.format(f.format_data(characteristic_times[0]))
    for characteristic_time in characteristic_times[1:]:
       current_label += r', ${}$'.format(f.format_data(characteristic_time))
    current_label += r'\} s; ' + '\n'

    if derivative_type == 1:
        current_label += r'$\alpha = $' + r'\{$%.2f$' % alpha[0]
        for alpha_i in alpha[1:]:
            current_label += r', $%.2f$' % alpha_i
        current_label += r'\}'

    current_label += r'$F = %.1f$ N' % external_force
    current_label += r'; $m = %.1f$ kg' % mass
    current_label += r'; $\Delta t = %.4f$ s' % delta_t
    current_label += r'; $u_0 = %.1f$ m' % initial_displacement


    # numerical calculation
    q = [q_0]
    u = [initial_displacement]
    F = [external_force]

    if static_dynamic == 0:
        # Static solution
        if derivative_type == 0:
            for j in range(n_time_steps):
                current_time = j * delta_t
                q_current_time = [0]

                for i in range(len(characteristic_times) - 1):
                    q_current_cell = delta_t / characteristic_times[i+1] * (moduli[i+1] * u[j] - q[j][i+1]) + q[j][i+1]
                    q_current_time.append(q_current_cell)
                q.append(q_current_time)

                if load_type_displacement == 0:
                    u_current_time = 1 / np.sum(moduli) * (external_force + np.sum(q[j]))
                elif load_type_displacement == 1:
                    u_current_time = initial_displacement
                else:
                    u_current_time = initial_displacement * np.sin(frequency*current_time)
                u.append(u_current_time)

                F_current_time = initial_displacement * np.sum(moduli) - np.sum(q[j])
                F.append(F_current_time)
        else:
            # weights
            weight_0 = [1] * len(alpha)
            weight = [weight_0]

            for j in range(1, n_time_steps+1):
                weight_current_time = [0]
                for i in range(1, len(alpha)):
                    # weight_current_time = [0]
                    weight_current_cell = (j - 1 - alpha[i]) / j * weight[j - 1][i]
                    weight_current_time.append(weight_current_cell)
                weight.append(weight_current_time)

            for j in range(n_time_steps):
                current_time = j * delta_t
                sum = 0
                # counting of the sum(w*q)
                q_current_time = [0]

                for i in range(1, len(moduli)):
                    # q_current_time = [0]
                    # sum = sum + q[j][i] * weight[len(q) - j][i]
                    sum = my_sum(i, j)
                    q_current_cell = (delta_t / characteristic_times[i]) ** alpha[i] * (moduli[i] * u[j] - q[j][i]) - sum
                    q_current_time.append(q_current_cell)
                q.append(q_current_time)

                if load_type_displacement == 0:
                    u_current_time = 1 / np.sum(moduli) * (external_force + np.sum(q[j]))
                elif load_type_displacement == 1:
                    u_current_time = initial_displacement
                else:
                    u_current_time = initial_displacement * np.sin(frequency*current_time)
                u.append(u_current_time)

                F_current_time = initial_displacement * np.sum(moduli) - np.sum(q[j])
                F.append(F_current_time)

    else:
        # Dynamic solution
        if derivative_type == 0:
            for j in range(n_time_steps):
                current_time = j * delta_t
                q_current_time = [0]
                for i in range(len(characteristic_times) - 1):
                    q_current_cell = delta_t / characteristic_times[i+1] * (moduli[i+1] * u[j] - q[j][i+1]) + q[j][i+1]
                    q_current_time.append(q_current_cell)
                q.append(q_current_time)

                if load_type_displacement == 0:
                    u_current_time = delta_t ** 2 / mass * (external_force + np.sum(q[j]) - u[j] * np.sum(moduli)) + 2 * u[j] - u[j-1]
                elif load_type_displacement == 1:
                    u_current_time = initial_displacement
                else:
                    u_current_time = initial_displacement * np.sin(frequency*current_time)
                u.append(u_current_time)

                F_current_time = initial_displacement * np.sum(moduli) - np.sum(q[j])
                F.append(F_current_time)
        else:
            # weights
            weight_0 = [1] * len(alpha)
            weight = [weight_0]

            for j in range(1, n_time_steps+1):
                weight_current_time = [0]
                for i in range(1, len(alpha)):
                    # weight_current_time = [0]
                    weight_current_cell = (j - 1 - alpha[i]) / j * weight[j - 1][i]
                    weight_current_time.append(weight_current_cell)
                weight.append(weight_current_time)

            for j in range(n_time_steps):
                current_time = j * delta_t
                sum = 0
                # counting of the sum(w*q)
                q_current_time = [0]
                for i in range(1, len(moduli)):
                    sum = my_sum(i, j)
                    q_current_cell = (delta_t / characteristic_times[i]) ** alpha[i] * (moduli[i] * u[j] - q[j][i]) - sum
                    q_current_time.append(q_current_cell)
                q.append(q_current_time)

                if load_type_displacement == 0:
                    u_current_time = delta_t ** 2 / mass * (external_force + np.sum(q[j]) - u[j] * np.sum(moduli)) + 2 * u[j] - u[j - 1]
                elif load_type_displacement == 1:
                    u_current_time = initial_displacement
                else:
                    u_current_time = initial_displacement * np.sin(frequency*current_time)
                u.append(u_current_time)

                F_current_time = initial_displacement * np.sum(moduli) - np.sum(q[j])
                F.append(F_current_time)

    # plotting
    plt.plot(x_values, u, label=current_label)
    # plt.plot(x_values, F, label=current_label)

plt.xlabel(r"$t$ [s]")
plt.ylabel(r"$u(t)$ [m]")
plt.legend()
plt.tight_layout()
# plt.savefig("plots/maxwell_chain_finite_difference.pdf")
# plt.savefig("plots/maxwell_chain_finite_difference.jpg", dpi=300)
plt.show()



